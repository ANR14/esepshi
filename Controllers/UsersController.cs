using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Repositories;

namespace esepshi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IUserRepository _userRepository;

        public UsersController(
            ILogger<UsersController> logger,
            IUserRepository userRepository)
        {
            _logger = logger;
            _userRepository = userRepository;
        }

        [HttpDelete("iin")]
        public async Task<string> DeleteUser(string iin)
        {
            var result = await _userRepository.Delete(iin);
            return result;
        }

        [HttpGet("iin")]
        public async Task<User> GetUser(string iin)
        {
            var result = await _userRepository.GetUserByIIN(iin);
            return result;
        }

        [HttpGet()]
        public async Task<List<User>> GetUsers()
        {
            var result = await _userRepository.GetUsers();
            return result;
        }

        [HttpPost]
        public async Task<ActionResult<User>> CreateUser([FromBody] User user)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            
            var result = await _userRepository.Create(user);
            return result;
        }

        [HttpPut]
        public async Task<ActionResult<string>> UpdateUser([FromBody] User user)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            
            var result = await _userRepository.Update(user);
            return result;
        }
    }
}