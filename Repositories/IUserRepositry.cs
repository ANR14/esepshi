using System;
using System.Collections.Generic;
using System.Linq;
using Models;
using Dapper;
using Npgsql;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Repositories
{
    public interface IUserRepository
    {
        Task<User> Create(User user);
        Task<string> Delete(string iin);
        Task<User> GetUserByIIN(string iin);
        Task<List<User>> GetUsers();
        Task<string> Update(User user);
    }

    public class UserRepository : IUserRepository
    {
        private readonly IConfiguration _config;
        
        public UserRepository(IConfiguration config)
        {
            _config = config;
        }

        public async Task<User> Create(User user)
        {            
            using (var db = new NpgsqlConnection(_config.GetConnectionString("postgres")))
            {
                var sqlQuery = "INSERT INTO Users (Name, Age) VALUES(@Name, @Age)";
                var result = await db.ExecuteAsync(sqlQuery, user);

                return result > 0 ? user : null;
            }
        }

        public async Task<string> Delete(string iin)
        {
            using (var db = new NpgsqlConnection(_config.GetConnectionString("postgres")))
            {
                var sqlQuery = "DELETE FROM Users WHERE Id = @id";
                var result = await db.ExecuteAsync(sqlQuery, new { iin });

                return result > 0 ? "User deleted" : "User not deleted";
            }
        }

        public async Task<User> GetUserByIIN(string iin)
        {
            using (var db = new NpgsqlConnection(_config.GetConnectionString("postgres")))
            {
                var result = await db.QueryAsync<User>("SELECT * FROM Users WHERE Id = @id", new { iin });

                return result.FirstOrDefault();
            }
        }

        public async Task<List<User>> GetUsers()
        {
            using (var db = new NpgsqlConnection(_config.GetConnectionString("postgres")))
            {
                var result = await db.QueryAsync<User>("SELECT * FROM Users");

                return result.ToList();
            }
        }

        public async Task<string> Update(User user)
        {
            using (var db = new NpgsqlConnection(_config.GetConnectionString("postgres")))
            {
                var sqlQuery = "UPDATE Users SET Name = @Name, Age = @Age WHERE Id = @Id";
                var result = await db.ExecuteAsync(sqlQuery, user);

                return result > 0 ? "User updated" : "User not updated";
            }
        }
    }
}