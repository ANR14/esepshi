using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Models
{
    public class User
    {
        [Key]
        [Required]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [RegularExpression(@"^[0-9]+$", ErrorMessage = "ИИН должен содержать только цифры!")]
        [StringLength(12, MinimumLength = 12, ErrorMessage = "ИИН должен содержать 12 цифр!")]
        [Required]
        [JsonProperty("iin")]
        public string IIN { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
        
        [JsonProperty("birthDate")]
        public DateTime BirthDate { get; set; }
    }
}